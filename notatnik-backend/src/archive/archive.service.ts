import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { ArchiveNote } from './interfaces/archive-note.interface';
import { CreateArchiveNoteDto } from './dto/create-archive-note.dto';
import { EditArchiveNoteDto } from './dto/edit-archive-note.dto';
import { Note } from '../notes/interfaces/note.interface';
import { CreateNoteDto } from '../notes/dto/create-note.dto';

@Injectable()
export class ArchiveService {
  constructor(    @Inject('NOTE_MODEL') private readonly noteModel: Model<Note>,
                  @Inject('ARCHIVE_NOTE_MODEL') private readonly archiveNoteModel: Model<ArchiveNote>) {}

  async create(createNoteDto: CreateArchiveNoteDto): Promise<ArchiveNote> {
    return this.archiveNoteModel.create(createNoteDto);
  }

  async findAll() {
    return this.archiveNoteModel.find().exec();
  }

  async findOneByNoteId(id: string) {
    return this.archiveNoteModel.findById(id)
  }

  async findByUserId(id: string) {
    return this.archiveNoteModel.find({user_id: id})
  }

  async editNote(id: string, editNoteDto: EditArchiveNoteDto) {
    return this.archiveNoteModel.findByIdAndUpdate(id, editNoteDto)
  }

  async deleteNote(id: string) {
    return this.archiveNoteModel.deleteOne({_id: id})
  }

  async moveToNotes(id: string) {
    let archiveNote = await this.archiveNoteModel.findById(id).exec();

    if (archiveNote) {
      const note: CreateNoteDto = {
        title: archiveNote.title,
        content: archiveNote.content,
        date: archiveNote.date,
        user_id: archiveNote.user_id,
        label: archiveNote.label
      };

      await this.noteModel.create(note);
      return this.deleteNote(id);
    } else {
      throw new HttpException('Note not found', HttpStatus.NOT_FOUND);
    }
  }

  deleteAllNotes(uid: string) {
    console.log(uid);
    return this.archiveNoteModel.deleteMany({user_id: uid})
  }
}
