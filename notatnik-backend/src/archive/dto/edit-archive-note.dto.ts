export class EditArchiveNoteDto {
  readonly title: string;
  readonly content: string;
  date: Date;
  label: string;
}