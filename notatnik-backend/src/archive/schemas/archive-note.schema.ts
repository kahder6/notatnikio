import * as mongoose from 'mongoose';

export const ArchiveNoteSchema : mongoose.Schema = new mongoose.Schema({
  title: String,
  date: Date,
  content: String,
  user_id: String,
  label: String
}, {
  versionKey: false
})