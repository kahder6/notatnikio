import { Mongoose } from 'mongoose';
import { ArchiveNoteSchema } from './schemas/archive-note.schema';

export const archiveProviders = [
  {
    provide: 'ARCHIVE_NOTE_MODEL',
    useFactory: (mongoose: Mongoose) => mongoose.model('ArchiveNote', ArchiveNoteSchema),
    inject: ['DATABASE_CONNECTION'],
  },
];