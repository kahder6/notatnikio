import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import { ArchiveService } from './archive.service';
import { CreateArchiveNoteDto } from './dto/create-archive-note.dto';
import { EditArchiveNoteDto } from './dto/edit-archive-note.dto';
import { AuthGuard } from '../auth/auth.guard';

@Controller('archive')
export class ArchiveController {
  constructor(private readonly notesService: ArchiveService) {}

  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard)
  @Post()
  createNote(@Body() createArchiveNoteDto: CreateArchiveNoteDto, @Request() req: any) {
    createArchiveNoteDto.date = new Date(Date.now());
    createArchiveNoteDto.user_id = req.user.sub;
    return this.notesService.create(createArchiveNoteDto);
  }

  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @Get()
  findAllNotes(@Request() req: any) {
    console.log(req.user);
    return this.notesService.findAll();
  }

  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @Get('nid/:id')
  findOneByNoteId(@Param() param) {
    return this.notesService.findOneByNoteId(param['id']);
  }

  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @Get('uid')
  findByUserId(@Request() req: any) {
    return this.notesService.findByUserId(req.user.sub);
  }

  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @Put('/:id')
  editNote(@Param() param, @Body() editArchiveNoteDto: EditArchiveNoteDto) {
    editArchiveNoteDto.date = new Date(Date.now());
    return this.notesService.editNote(param['id'], editArchiveNoteDto);
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(AuthGuard)
  @Delete('/:id')
  deleteNote(@Param() param) {
    return this.notesService.deleteNote(param['id']);
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(AuthGuard)
  @Delete()
  deleteAllNotes(@Request() req: any) {
    return this.notesService.deleteAllNotes(req.user.sub);
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(AuthGuard)
  @Get('move_to_notes/:id')
  moveToArchive(@Param() param) {
    return this.notesService.moveToNotes(param['id']);
  }
}
