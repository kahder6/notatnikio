import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { ArchiveController } from './archive.controller';
import { ArchiveService } from './archive.service';
import { archiveProviders } from './archive.providers';
import { NotesService } from '../notes/notes.service';
import { notesProviders } from '../notes/notes.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [ArchiveController],
  providers: [NotesService, ArchiveService, ...archiveProviders, ...notesProviders]
}) export class ArchiveModule {}