import { Document } from 'mongoose';

export interface ArchiveNote extends Document {
  title: string;
  date: Date;
  content: string;
  user_id: string;
  label: string;
}