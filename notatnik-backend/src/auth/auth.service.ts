import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from '../users/dto/create-user.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signIn(username, pass) {
    const [user] = await this.usersService.findOne(username);
    if (!user) {
      throw new HttpException("User doesn't exists", HttpStatus.BAD_REQUEST);
    }
    const isMatch = await bcrypt.compare(pass, user?.password);

    if (!isMatch) {
      throw new UnauthorizedException();
    }
    const payload = { sub: user._id, username: user.username };

    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }

  async signUp(signUpDto) {
    const user = await this.usersService.findOne(signUpDto.username);

    if (user.length) {
      throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
    } else if (signUpDto.password !== signUpDto.passwordConfirmation) {
      throw new HttpException(
        "Passwords doesn't match",
        HttpStatus.BAD_REQUEST,
      );
    } else if(signUpDto.password === '' || signUpDto.username === ''){
      throw new HttpException(
        "Username or password is empty",
        HttpStatus.BAD_REQUEST,
      );
    } else {
      const hash = await bcrypt.hash(signUpDto.password, 12);
      const createUserDto: CreateUserDto = {
        username: signUpDto.username,
        password: hash,
      };
      return await this.usersService.create(createUserDto);
    }
  }

}
