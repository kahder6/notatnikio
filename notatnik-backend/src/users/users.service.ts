import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';

// This should be a real class/interface representing a user entity
export type User = any;

@Injectable()
export class UsersService {
  constructor(@Inject('USER_MODEL') private readonly userModel: Model<User>) {
  }

  async findOne(username: string): Promise<User | undefined> {
    return this.userModel.find({username}).exec();
  }

  async create(createUserDto: CreateUserDto) {
    return this.userModel.create(createUserDto)
  }
}