import * as mongoose from 'mongoose';
export const UserSchema: mongoose.Schema = new mongoose.Schema({
  username: String,
  password: String
}, {
  versionKey: false
})