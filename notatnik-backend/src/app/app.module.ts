import { Module } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { NotesModule } from '../notes/notes.module';
import { ArchiveModule } from '../archive/archive.module';

@Module({ imports: [AuthModule, NotesModule, ArchiveModule] })
export class AppModule {}
