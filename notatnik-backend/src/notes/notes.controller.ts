import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import { NotesService } from './notes.service';
import { CreateNoteDto } from './dto/create-note.dto';
import { AuthGuard } from '../auth/auth.guard';
import { EditNoteDto } from './dto/edit-note.dto';

@Controller('notes')
export class NotesController {
  constructor(private readonly notesService: NotesService) {}

  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard)
  @Post()
  createNote(@Body() createNoteDto: CreateNoteDto, @Request() req: any) {
    createNoteDto.date = new Date(Date.now());
    createNoteDto.user_id = req.user.sub;
    return this.notesService.create(createNoteDto);
  }

  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @Get()
  findAllNotes(@Request() req: any) {
    return this.notesService.findAll();
  }

  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @Get('nid/:id')
  findOneByNoteId(@Param() param) {
    return this.notesService.findOneByNoteId(param['id']);
  }

  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @Get('uid')
  findByUserId(@Request() req: any) {
    return this.notesService.findByUserId(req.user.sub);
  }

  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard)
  @Put('/:id')
  editNote(@Param() param, @Body() editNoteDto: EditNoteDto) {
    editNoteDto.date = new Date(Date.now());
    return this.notesService.editNote(param['id'], editNoteDto);
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(AuthGuard)
  @Delete('/:id')
  deleteNote(@Param() param) {
    return this.notesService.deleteNote(param['id']);
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(AuthGuard)
  @Delete()
  deleteAllNotes(@Request() req: any) {
    return this.notesService.deleteAllNotes(req.user.sub);
  }

  @HttpCode(HttpStatus.NO_CONTENT)
  @UseGuards(AuthGuard)
  @Get('move_to_archive/:id')
  moveToArchive(@Param() param) {
    return this.notesService.moveToArchive(param['id'])
  }
}
