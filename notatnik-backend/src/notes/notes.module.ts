import { Module } from '@nestjs/common';
import { DatabaseModule } from '../database/database.module';
import { NotesController } from './notes.controller';
import { NotesService } from './notes.service';
import { notesProviders } from './notes.providers';
import { ArchiveService } from '../archive/archive.service';
import { archiveProviders } from '../archive/archive.providers';

@Module({
  imports: [DatabaseModule],
  controllers: [NotesController],
  providers: [NotesService, ArchiveService, ...archiveProviders, ...notesProviders]
}) export class NotesModule {}