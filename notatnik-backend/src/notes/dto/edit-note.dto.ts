export class EditNoteDto {
  readonly title: string;
  readonly content: string;
  date: Date;
  label: string;
}