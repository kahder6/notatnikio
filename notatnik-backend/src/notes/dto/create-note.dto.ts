export class CreateNoteDto {
  readonly title: string;
  readonly content: string;
  user_id: string;
  date: Date;
  label: string;
}

