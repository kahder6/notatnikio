import { Document } from 'mongoose';

export interface Note extends Document {
  title: string;
  date: Date;
  content: string;
  user_id: string;
  label: string;
}