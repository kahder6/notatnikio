import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Note } from './interfaces/note.interface';
import { CreateNoteDto } from './dto/create-note.dto';
import { EditNoteDto } from './dto/edit-note.dto';
import { CreateArchiveNoteDto } from '../archive/dto/create-archive-note.dto';
import { ArchiveNote } from '../archive/interfaces/archive-note.interface';

@Injectable()
export class NotesService {
  constructor(
    @Inject('NOTE_MODEL') private readonly noteModel: Model<Note>,
    @Inject('ARCHIVE_NOTE_MODEL') private readonly archiveNoteModel: Model<ArchiveNote>
  ) {}

  async create(createNoteDto: CreateNoteDto): Promise<Note> {
    return this.noteModel.create(createNoteDto);
  }

  async findAll() {
    return this.noteModel.find().exec();
  }

  async findOneByNoteId(id: string) {
    return this.noteModel.findById(id);
  }

  async findByUserId(id: string) {
    return this.noteModel.find({ user_id: id });
  }

  async editNote(id: string, editNoteDto: EditNoteDto) {
    return this.noteModel.findByIdAndUpdate(id, editNoteDto);
  }

  async deleteNote(id: string) {
    return this.noteModel.deleteOne({ _id: id });
  }

  async moveToArchive(id: string) {
    let note = await this.noteModel.findById(id).exec();

    if (note) {
      const archiveNote: CreateArchiveNoteDto = {
        title: note.title,
        content: note.content,
        date: note.date,
        user_id: note.user_id,
        label: note.label
      };

      await this.archiveNoteModel.create(archiveNote);
      return this.deleteNote(id);
    } else {
      throw new HttpException('Note not found', HttpStatus.NOT_FOUND);
    }
  }

  deleteAllNotes(uid: string) {
    return this.noteModel.deleteMany({user_id: uid})
  }
}
