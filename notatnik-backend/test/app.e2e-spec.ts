import { Test,TestingModule  } from '@nestjs/testing';
import { AuthService } from '../src/auth/auth.service';
import { UsersService } from '../src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { CreateNoteDto } from '../src/notes/dto/create-note.dto';
import { EditNoteDto } from '../src/notes/dto/edit-note.dto';
import { NotesService } from '../src/notes/notes.service';
import { HttpException, HttpStatus, UnauthorizedException } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { Model } from 'mongoose';
import { Note } from '../src/notes/interfaces/note.interface';
import { ArchiveNote } from '../src/archive/interfaces/archive-note.interface';


describe('AuthService', () => {
  let service: AuthService;
  let usersService: UsersService;
  let jwtService: JwtService;

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: {
            findOne: jest.fn(),
            create: jest.fn(),
          },
        },
        {
          provide: JwtService,
          useValue: {
            signAsync: jest.fn(),
          },
        },
      ],
    }).compile();

    service = moduleRef.get<AuthService>(AuthService);
    usersService = moduleRef.get<UsersService>(UsersService);
    jwtService = moduleRef.get<JwtService>(JwtService);
  });

  describe('signIn', () => {
    it('should throw HttpException when user does not exist', async () => {
      jest.spyOn(usersService, 'findOne').mockResolvedValue([]);

      await expect(service.signIn('testUser', 'testPassword')).rejects.toThrowError(
        new HttpException("User doesn't exists", HttpStatus.BAD_REQUEST),
      );
    });

    it('should throw UnauthorizedException when password does not match', async () => {
      const user = { _id: '648efd4cd5668c5194935a29', username: 'gfds@gfds.plr', password: '$2b$12$tbInxUW3c60KjrgxBQH5leKEWB2tJ6PvfAxmtKB1nuknKUW7BL2om' };
      jest.spyOn(usersService, 'findOne').mockResolvedValue([user]);
      jest.spyOn(bcrypt, 'compare').mockImplementation(() => Promise.resolve(false));

      await expect(service.signIn('gfds@gfds.pl', 'testPassword')).rejects.toThrowError(UnauthorizedException);
    });

    it('should generate and return access token when credentials are valid', async () => {
      const user = { _id: '1', username: 'gfds@gfds.pl', password: '$2b$12$tbInxUW3c60KjrgxBQH5leKEWB2tJ6PvfAxmtKB1nuknKUW7BL2om' };
      const accessToken = 'testAccessToken';
      const signInDto = { username: 'gfds@gfds.pl', password: '$2b$12$tbInxUW3c60KjrgxBQH5leKEWB2tJ6PvfAxmtKB1nuknKUW7BL2om' };

      jest.spyOn(usersService, 'findOne').mockResolvedValue([user]);
      jest.spyOn(bcrypt, 'compare').mockImplementation(() => Promise.resolve(true));
      jest.spyOn(jwtService, 'signAsync').mockResolvedValue(accessToken);

      const result = await service.signIn(signInDto.username, signInDto.password);

      expect(usersService.findOne).toHaveBeenCalledWith(signInDto.username);
      expect(bcrypt.compare).toHaveBeenCalledWith(signInDto.password, user.password);
      expect(jwtService.signAsync).toHaveBeenCalledWith({ sub: user._id, username: user.username });
      expect(result).toEqual({ access_token: accessToken });
    });
  });

  describe('signUp', () => {
    it('should throw HttpException when user already exists', async () => {
      const signUpDto = { username: 'testUser', password: 'testPassword', passwordConfirmation: 'testPassword' };
      jest.spyOn(usersService, 'findOne').mockResolvedValue([{ username: 'testUser' }]);

      await expect(service.signUp(signUpDto)).rejects.toThrowError(
        new HttpException('User already exists', HttpStatus.BAD_REQUEST),
      );
    });

    it("should throw HttpException when passwords don't match", async () => {
      const signUpDto = { username: 'testUser', password: 'testPassword', passwordConfirmation: 'differentPassword' };
      jest.spyOn(usersService, 'findOne').mockResolvedValue([]);
  
      await expect(service.signUp(signUpDto)).rejects.toThrowError(
        new HttpException("Passwords doesn't match", HttpStatus.BAD_REQUEST),
      );
    });

    it("should throw HttpException when username or password is empty", async () => {
      const signUpDto = { username: '', password: 'testPassword', passwordConfirmation: 'testPassword' };
      jest.spyOn(usersService, 'findOne').mockResolvedValue([]);
  
      await expect(service.signUp(signUpDto)).rejects.toThrowError(
        new HttpException("Username or password is empty", HttpStatus.BAD_REQUEST),
      );
    });

    it('should create a new user and return the result', async () => {
      const signUpDto = { username: 'testUser', password: 'testPassword', passwordConfirmation: 'testPassword' };
      const createdUser = { id: '1', username: 'testUser', password: 'hashedPassword' };

      jest.spyOn(usersService, 'findOne').mockResolvedValue([]);
      jest.spyOn(bcrypt, 'hash').mockImplementation(async () => 'hashedPassword');
      jest.spyOn(usersService, 'create').mockResolvedValue(createdUser);

      const result = await service.signUp(signUpDto);

      expect(usersService.findOne).toHaveBeenCalledWith(signUpDto.username);
      expect(bcrypt.hash).toHaveBeenCalledWith(signUpDto.password, 12);
      expect(usersService.create).toHaveBeenCalledWith({ username: signUpDto.username, password: 'hashedPassword' });
      expect(result).toEqual(createdUser);
    });
  });
});


describe('NotesService', () => {
  let service: NotesService;
  let noteModel: Model<Note>;
  let archiveNoteModel: Model<ArchiveNote>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NotesService,
        {
          provide: 'NOTE_MODEL',
          useValue: {
            create: jest.fn().mockResolvedValue('mockNote'),
            find: jest.fn().mockReturnValue({
              exec: jest.fn().mockResolvedValue(['mockNote']),
            }),
            findById: jest.fn().mockResolvedValue('mockNote'),
            findByIdAndUpdate: jest.fn().mockResolvedValue('mockNote'),
            deleteOne: jest.fn().mockResolvedValue(true),
            deleteMany: jest.fn().mockResolvedValue(true),
          },
        },
        {
          provide: 'ARCHIVE_NOTE_MODEL',
          useValue: {
            create: jest.fn().mockResolvedValue('mockArchiveNote'),
          },
        },
      ],
    }).compile();
  
    service = module.get<NotesService>(NotesService);
    noteModel = module.get<Model<Note>>('NOTE_MODEL');
    archiveNoteModel = module.get<Model<ArchiveNote>>('ARCHIVE_NOTE_MODEL');
  });

  describe('create', () => {
    it('should create a new note', async () => {
      const createNoteDto: CreateNoteDto = {
        title: 'Test Note',
        content: 'Test Content',
        date: new Date(),
        user_id: 'user123',
      };

      const result = await service.create(createNoteDto);

      expect(noteModel.create).toHaveBeenCalledWith(createNoteDto);
      expect(result).toEqual('mockNote');
    });
  });

  describe('findAll', () => {
    it('should return all notes', async () => {
      const result = await service.findAll();

      expect(noteModel.find).toHaveBeenCalled();
      expect(result).toEqual(['mockNote']);
    });
  });

  describe('findOneByNoteId', () => {
    it('should return a note by note id', async () => {
      const noteId = 'note123';

      const result = await service.findOneByNoteId(noteId);

      expect(noteModel.findById).toHaveBeenCalledWith(noteId);
      expect(result).toEqual('mockNote');
    });
  });

  describe('findByUserId', () => {
    it('should return notes by user id', async () => {
      const userId = 'user123';
      const mockResult = ['mockNote'];
    
      jest.spyOn(noteModel, 'find').mockReturnValue(Promise.resolve(mockResult) as any);
    
      const result = await service.findByUserId(userId);
    
      expect(noteModel.find).toHaveBeenCalledWith({ user_id: userId });
      expect(result).toEqual(mockResult);
    });
    
    
  });

  describe('editNote', () => {
    it('should edit a note', async () => {
      const noteId = 'note123';
      const editNoteDto: EditNoteDto = {
        title: 'Updated Note Title',
        content: 'Updated Note Content',
        date: new Date()
      };

      const result = await service.editNote(noteId, editNoteDto);

      expect(noteModel.findByIdAndUpdate).toHaveBeenCalledWith(
        noteId,
        editNoteDto,
      );
      expect(result).toEqual('mockNote');
    });
  });

  describe('deleteNote', () => {
    it('should delete a note', async () => {
      const noteId = 'note123';

      await service.deleteNote(noteId);

      expect(noteModel.deleteOne).toHaveBeenCalledWith({ _id: noteId });
    });
  });

  describe('moveToArchive', () => {
    it('should move a note to archive', async () => {
      const noteId = 'note123';
      const note = {
        _id: noteId,
        title: 'Test Note',
        content: 'Test Content',
        date: new Date(),
        user_id: 'user123',
      };
      const createArchiveNoteDto = {
        title: note.title,
        content: note.content,
        date: note.date,
        user_id: note.user_id,
      };
  
      jest.spyOn(noteModel, 'findById').mockReturnValueOnce({
        exec: jest.fn().mockResolvedValueOnce(note),
      } as any); // Ignore type checking for exec
  
      await service.moveToArchive(noteId);
  
      expect(noteModel.findById).toHaveBeenCalledWith(noteId);
      expect(archiveNoteModel.create).toHaveBeenCalledWith(createArchiveNoteDto);
      expect(noteModel.deleteOne).toHaveBeenCalledWith({ _id: noteId });
  
      jest.restoreAllMocks();
    });
  });

    it('should throw an exception if note not found', async () => {
  const noteId = 'note123';

  jest.spyOn(noteModel, 'findById').mockReturnValueOnce({
    exec: jest.fn().mockResolvedValueOnce(null),
  } as any); // Ignore type checking for exec

  await expect(service.moveToArchive(noteId)).rejects.toThrowError('Note not found');

  expect(noteModel.findById).toHaveBeenCalledWith(noteId);
  expect(archiveNoteModel.create).not.toHaveBeenCalled();
  expect(noteModel.deleteOne).not.toHaveBeenCalled();
});
  

  describe('deleteAllNotes', () => {
    it('should delete all notes by user id', async () => {
      const userId = 'user123';

      await service.deleteAllNotes(userId);

      expect(noteModel.deleteMany).toHaveBeenCalledWith({ user_id: userId });
    });
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

});