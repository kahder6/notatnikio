export interface IAuthorizationContext {
    isAuthorized: boolean,
    setIsAuthorized: (st: boolean) => void
}

export interface INote {
    _id: number,
    title: string,
    content: string,
    date: string,
    label: string
}

export interface IAddNote {
    title: string,
    content: string,
    label: string
}

export interface ILoginCredentials {
    username: string,
    password: string
}

export interface IRegisterCredentials {
    username: string,
    password: string,
    passwordConfirmation: string
}
