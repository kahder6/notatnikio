import React, { useState } from 'react'
import { Paper, Grid, TextField, Button, Link as MLink } from '@mui/material'
import { Link, useNavigate } from 'react-router-dom'
import { IAddNote } from '../../Models';
import axios from 'axios';


function AddNote() {
    const navigate = useNavigate();
    const [note, setNote] = useState<IAddNote>({ title: "", content: "", label: "" })

    const handleAddNote = () => {
        axios.post(`${process.env.REACT_APP_API_URL}notes`, note, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then(res => {
            if (res.status === 201) navigate('/notes')
        })
    }

    return (
        <div className='formContainer'>
            <Paper variant="elevation" elevation={2} style={{ padding: "2rem", textAlign: "left" }} >
                <Grid container spacing={2}>
                    <Grid item sm={12}><h1>Dodaj notatkę</h1></Grid>
                    <Grid item sm={12}>
                        <TextField autoComplete='off' label="Tytuł" variant='outlined' fullWidth={true} onChange={e => {
                            setNote((o: IAddNote) => { return { ...o, title: e.target.value } })
                        }} />
                    </Grid>
                    <Grid item sm={12}>
                        <TextField label="Treść" placeholder='Aa...' multiline={true} rows={5} variant='outlined' fullWidth={true} onChange={e => {
                            setNote((o: IAddNote) => { return { ...o, content: e.target.value } })
                        }} />
                    </Grid>
                    <Grid item sm={12}>
                        <TextField autoComplete='off' label="Kategoria" variant='outlined' fullWidth={true} onChange={e => {
                            setNote((o: IAddNote) => { return { ...o, label: e.target.value } })
                        }} />
                    </Grid>
                    <Grid item sm={12}><Button variant='contained' onClick={handleAddNote}>Dodaj notatkę</Button></Grid>
                    <Grid item sm={12}><MLink component={Link} to='/notes'>Powrót do listy notatek</MLink></Grid>
                </Grid>
            </Paper>
        </div>
    )
}

export default AddNote
