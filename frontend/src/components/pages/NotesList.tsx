import React, { useContext, useEffect, useState } from 'react'
import { INote } from '../../Models'
import NoteItem from '../common/NoteItem'
import { Button, MenuItem, Select, SelectChangeEvent } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import { AuthorizationContext } from '../../App'

function NotesList() {
    const navigate = useNavigate()
    const [notes, setNotes] = useState<INote[]>([])
    const [filteredNotes, setFilteredNotes] = useState<INote[]>([])
    const { setIsAuthorized } = useContext(AuthorizationContext)
    const [category, setCategory] = useState<string>("all");
    const [labels, setLabels] = useState<string[]>([])

    const handleDeleteNote = (id: number) => {
        axios.delete(`${process.env.REACT_APP_API_URL}notes/${id}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then(res => {
            setNotes(n => {
                return n.filter(i => i._id !== id)
            });
            setFilteredNotes(n => {
                return n.filter(i => i._id !== id)
            });
        })
    }

    const handleEditNote = (id: number, content: string) => {
        const editedNotes = [...notes]
        const pos = editedNotes.find(e => e._id === id)
        if (pos) {
            pos.content = content;

            axios.put(`${process.env.REACT_APP_API_URL}notes/${id}`, pos, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            }).then(res => {
                setNotes(editedNotes)
            }).catch(() => { })
        }
    }

    const handleMoveNote = (id: number) => {
        axios.get(`${process.env.REACT_APP_API_URL}notes/move_to_archive/${id}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then((res) => {
            handleDeleteNote(id)
        }).catch(() => { })
    }

    const handleCategoryChange = (e: SelectChangeEvent) => {
        let label = e.target.value
        setCategory(label)
        setFilteredNotes(label === 'all' ? notes : notes.filter((n: INote) => n.label === label))

    }

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_API_URL}notes/uid`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then((res) => {
            if (res.status === 401) {
                setIsAuthorized(false)
                navigate('/')
            }
            setNotes(res.data)
            setFilteredNotes(res.data)
            const _labels = new Set(res.data.filter((item: INote) => item.label != undefined).map((item: INote) => item.label))
            const stringLabels: string[] = Array.from(_labels) as string[]
            setLabels(stringLabels)

        }).catch(e => {
            if (e.response.status === 401) {
                setIsAuthorized(false)
                navigate('/')
            }
        })
    }, [setIsAuthorized, navigate])

    return (
        <>
            <h1>Notatki</h1>

            <Button style={{ margin: "1rem" }} color='success' variant='contained' onClick={() => navigate('/addNote')}>+ Dodaj notatkę</Button>

            <Select value={category} onChange={handleCategoryChange}>
                <MenuItem value={'all'}>wszystkie</MenuItem>
                {labels.map((i) => {
                    return <MenuItem value={i} key={i}>{i}</MenuItem>
                })}
            </Select>


            {filteredNotes.map((k, v) => {
                return <NoteItem note={k} key={v} handleDeleteNote={handleDeleteNote} handleEditNote={handleEditNote} handleMoveNote={handleMoveNote} moveToArchive={true} />
            })}
        </>
    )
}

export default NotesList
