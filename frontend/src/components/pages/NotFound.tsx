import React from 'react'

function NotFound() {

    return (
        <div style={{ textAlign: "center" }}>
            <h1>Błąd 404 - Nie znaleziono strony</h1>
        </div>
    )
}

export default NotFound
