import { useState, useContext, useEffect } from 'react'
import { Paper, Grid, Box, TextField, Button, Link as MLink } from '@mui/material'
import { Link, useNavigate } from 'react-router-dom'
import { ILoginCredentials } from '../../Models'
import axios, { AxiosError } from 'axios'
import { AuthorizationContext } from '../../App'

function Login() {
    const [login, setLogin] = useState<ILoginCredentials>({ username: "", password: "" })
    const [error, setError] = useState<string>();
    const navigate = useNavigate();
    const { isAuthorized, setIsAuthorized } = useContext(AuthorizationContext)

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        axios.post(`${process.env.REACT_APP_API_URL}auth/login`, login).then(res => {
            console.log(res);

            if (res.status === 200) {
                setIsAuthorized(true)
                localStorage.setItem("token", res.data.access_token)
                navigate('/notes');
            }
        }).catch((e: Error | AxiosError) => {
            if (axios.isAxiosError(e)) {
                if (e.response?.status === 400) {
                    setError(e.response?.data.message)
                } else if (e.response?.status === 401) {
                    setError("Zła nazwa użytkownika lub hasło")
                }
            }
            console.log("err");
            console.log(e);
        })
    }

    useEffect(() => {
        if (isAuthorized) navigate('/');
    }, [isAuthorized, navigate])

    return (
        <div className='formContainer'>
            <Box style={{ display: "inline-block", width: "60%" }}>
                <Paper variant="elevation" elevation={2} style={{ padding: "2rem", textAlign: "left" }} >
                    <form onSubmit={handleSubmit}>
                        <Grid container spacing={2}>
                            <Grid item sm={12}><h1>Zaloguj się</h1></Grid>
                            <Grid item sm={12}>
                                <TextField label="email" variant='outlined' fullWidth={true} onChange={(e) => {
                                    setLogin((l: ILoginCredentials) => { return { ...l, username: e.target.value } })
                                }} />
                            </Grid>
                            <Grid item sm={12}>
                                <TextField type='password' label="hasło" variant='outlined' fullWidth={true} onChange={(e) => {
                                    setLogin((l: ILoginCredentials) => { return { ...l, password: e.target.value } })
                                }} />
                            </Grid>
                            <Grid item sm={12}><Button type='submit' variant='contained'>Zaloguj się</Button></Grid>
                            <Grid item sm={12}><MLink component={Link} to='/register'>Nie masz konta? Zarejestruj się!</MLink></Grid>
                        </Grid>
                    </form>
                    {error !== '' ? <div className="error">{error}</div> : ''}
                </Paper>
            </Box>
        </div >
    )
}

export default Login