import { Button, Grid } from '@mui/material'
import React, { useContext } from 'react'
import { useNavigate } from 'react-router-dom';
import { AuthorizationContext } from '../../App';

function Home() {
    const navigate = useNavigate();
    const { isAuthorized } = useContext(AuthorizationContext)
    return (
        <div style={{ marginTop: "2rem", textAlign: "center" }}>
            <h1>NotepadIO</h1>
            Najlepsze narzędzie do tworzenia notatek online!

            {!isAuthorized ?
                <Grid container spacing={2} className='mt-5'>
                    <Grid item sm={12}>
                        <Button variant="outlined" onClick={() => {
                            navigate("/login")
                        }}>Zaloguj się</Button>
                    </Grid>
                    <Grid item sm={12}>
                        <Button variant="outlined" onClick={() => {
                            navigate("/register")
                        }}>Zarejestruj się</Button>
                    </Grid>
                </Grid>
                :
                <Grid container spacing={2} className='mt-5'>
                    <Grid item sm={12}>
                        <Button variant="outlined" onClick={() => {
                            navigate("/notes")
                        }}>Moje notatki</Button>
                    </Grid>
                </Grid>}
        </div>
    )
}

export default Home
