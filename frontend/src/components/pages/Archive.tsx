import { useContext, useEffect, useState } from 'react'
import { INote } from '../../Models'
import NoteItem from '../common/NoteItem'
import { Button } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import axios from 'axios'
import { AuthorizationContext } from '../../App'

function Archive() {
    const navigate = useNavigate()
    const [notes, setNotes] = useState<INote[]>([])
    const { setIsAuthorized } = useContext(AuthorizationContext)

    const handleDeleteNote = (id: number) => {
        axios.delete(`${process.env.REACT_APP_API_URL}archive/${id}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then(res => {
            setNotes(n => {
                return n.filter(i => i._id !== id)
            });
        })
    }

    const handleEditNote = (id: number, content: string) => {
        const editedNotes = [...notes]
        const pos = editedNotes.find(e => e._id === id)
        if (pos) {
            pos.content = content;

            axios.put(`${process.env.REACT_APP_API_URL}archive/${id}`, pos, {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem("token")}`
                }
            }).then(res => {
                setNotes(editedNotes)
            }).catch(() => {})
        }
    }

    const handleMoveNote = (id: number) => {
        axios.get(`${process.env.REACT_APP_API_URL}archive/move_to_notes/${id}`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then((res) => {
            handleDeleteNote(id)
        }).catch(() => { })
    }

    const handleClearArchive = () => {
        axios.delete(`${process.env.REACT_APP_API_URL}archive`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then((res) => {
            setNotes([])
        }).catch(() => { })
    }

    useEffect(() => {
        axios.get(`${process.env.REACT_APP_API_URL}archive/uid`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then((res) => {
            setNotes(res.data)

        }).catch(e => {
            console.log(e);
            if (e.response.status === 401) {
                setIsAuthorized(false)
                navigate('/')
            }
        })
    }, [setIsAuthorized, navigate])

    return (
        <>
            <h1>Archiwum notatek</h1>

            <Button className='m-3' color='error' variant='contained' onClick={handleClearArchive}>Wyczyść archiwum</Button>

            {notes.map((k, v) => {
                return <NoteItem note={k} key={v} handleDeleteNote={handleDeleteNote} handleEditNote={handleEditNote} handleMoveNote={handleMoveNote} moveToArchive={false} />
            })}
        </>
    )
}

export default Archive
