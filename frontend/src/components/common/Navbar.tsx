import React, { useContext } from 'react'
import { Button, Grid } from "@mui/material"
import { BiNotepad } from "react-icons/bi"
import { useNavigate } from 'react-router-dom'
import { AuthorizationContext } from '../../App'
import axios from 'axios'

function Navbar() {
    const navigate = useNavigate();
    const { isAuthorized, setIsAuthorized } = useContext(AuthorizationContext)
    return (
        <nav>
            <div id="logo" onClick={() => navigate('/')}><BiNotepad /> <div>NotepadIO</div></div>
            <div id="buttons">
                {isAuthorized ?
                    <Grid container spacing={2}>
                        <Grid item>
                            <Button variant='contained' onClick={() => {
                                navigate('/notes')
                            }}>Notatki</Button>
                        </Grid>
                        <Grid item>
                            <Button variant='contained' onClick={() => {
                                navigate('/archive')
                            }}>Archiwum</Button>
                        </Grid>
                        <Grid item>
                            <Button variant='contained' onClick={() => {
                                axios.post(`${process.env.REACT_APP_API_URL}auth/logout`, {
                                    headers: {
                                        Authorization: `Bearer ${localStorage.getItem("token")}`
                                    }
                                }).catch(() => {}).finally(() => {
                                    // handleDeleteNote(id)
                                    setIsAuthorized(false);
                                    localStorage.removeItem("token")
                                    navigate('/')
                                })

                            }}>Wyloguj się</Button>
                        </Grid>
                    </Grid>
                    :
                    <Grid container spacing={2}>
                        <Grid item>
                            <Button variant='contained' onClick={() => {
                                navigate('/login')
                            }}>Zaloguj się</Button>
                        </Grid>
                        <Grid item>
                            <Button variant='contained' onClick={() => {
                                navigate('/register')
                            }}>Zarejestruj się</Button>
                        </Grid>
                    </Grid>
                }
            </div>
        </nav>
    )
}

export default Navbar
