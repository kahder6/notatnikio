import React, { useState, useEffect } from 'react'
import { Accordion, AccordionSummary, AccordionDetails, Typography, Button, TextField, Chip, Stack } from '@mui/material'
import { INote } from '../../Models'
import { SlArrowDown } from 'react-icons/sl'

interface Props {
    note: INote,
    moveToArchive: boolean,
    handleDeleteNote: (id: number) => void,
    handleEditNote: (id: number, content: string) => void,
    handleMoveNote: (id: number) => void,
}


function NoteItem(props: Props) {
    const [editing, setEditing] = useState<boolean>(false);
    const [content, setContent] = useState<string>("");

    const handleEdit = () => {
        if (editing) {
            props.handleEditNote(props.note._id, content)
        }
        setEditing(e => !e)
    }

    const handleMove = () => {
        const id = props.note._id
        // console.log("MOVE note id = " + id);
        props.handleMoveNote(id)
    }

    const handleContentChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setContent(e.target.value)
    }

    const formatDate = (date?: string) => {
        if (!date) return "Unknown"
        const d = new Date(date)
        return d.getFullYear() + "-" + ((d.getMonth() + 1) < 10 ? "0" + (d.getMonth() + 1) : (d.getMonth() + 1)) + "-" + (d.getDate() < 10 ? "0" : "") + d.getDate() + " " + (d.getHours() < 10 ? "0" : "") + d.getHours() + ":" + (d.getMinutes() < 10 ? "0" : "") + d.getMinutes() + ":" + (d.getSeconds() < 10 ? "0" : "") + d.getSeconds()
    }

    useEffect(() => {
        setContent(props.note.content)
    }, [props])

    return (
        <Accordion TransitionProps={{ unmountOnExit: true }}>
            <AccordionSummary expandIcon={<SlArrowDown />} style={{ justifyContent: "center" }}>
                <Typography component="div">
                    <span style={{ verticalAlign: "middle" }}>{props.note.title}</span> {props.note.label ? <Chip label={props.note.label} style={{ marginLeft: "1rem" }} size='small' /> : ""}
                    <span style={{ fontSize: "0.7rem", color: "#666", marginLeft: "1rem", verticalAlign: "middle" }}>{formatDate(props.note.date)}</span>
                </Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Typography component={'div'}>
                    {editing ?
                        <TextField value={content} fullWidth={true} multiline onChange={handleContentChange} />
                        : <div className="m-3" dangerouslySetInnerHTML={{ __html: content.replace(/\n/g, '<br />') }}></div>}
                </Typography>
                <div className="mt-5">
                    <Button variant='contained' style={{ marginRight: "1rem" }} onClick={handleEdit}>Edytuj</Button>
                    <Button variant='contained' style={{ marginRight: "1rem" }} onClick={handleMove}>{props.moveToArchive ? "Archiwizuj" : "Przywróć"}</Button>
                    <Button variant='contained' color="error" onClick={() => props.handleDeleteNote(props.note._id)}>Usuń</Button>
                </div>
            </AccordionDetails>
        </Accordion >
    )
}

export default NoteItem
