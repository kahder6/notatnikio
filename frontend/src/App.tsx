import React, { useEffect, useState, createContext } from 'react';
import { createTheme, ThemeProvider } from "@mui/material"
import { BrowserRouter, Routes, Route } from "react-router-dom"
import Navbar from './components/common/Navbar';
import Home from './components/pages/Home';
import './App.css';
import NotFound from './components/pages/NotFound';
import Login from './components/pages/Login';
import Register from './components/pages/Register';
import NotesList from './components/pages/NotesList';
import AddNote from './components/pages/AddNote';
import Archive from './components/pages/Archive';
import { IAuthorizationContext } from './Models';

export const AuthorizationContext = createContext<IAuthorizationContext>({ isAuthorized: false, setIsAuthorized: () => {} });

function App() {

  const theme = createTheme({
    palette: {
      primary: {
        main: "#F56A36",
      },
      secondary: {
        main: "#E8B84F"
      }
    },
    typography: {
      button: {
        textTransform: 'none'
      }
    }
  })

  const [isAuthorized, setIsAuthorized] = useState<boolean>(false)

  useEffect(() => {
    if (localStorage.getItem("token")) setIsAuthorized(true)
  }, [])


  return (
    <AuthorizationContext.Provider value={{ isAuthorized: isAuthorized, setIsAuthorized: setIsAuthorized }}>
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <Navbar />
          <div id="container">
            <Routes>
              <Route path='/' element={<Home />} />
              <Route path='/login' element={<Login />} />
              <Route path='/register' element={<Register />} />

              <Route path='/notes' element={<NotesList />} />
              <Route path='/addNote' element={<AddNote />} />
              <Route path="/archive" element={<Archive />} />

              <Route path="*" element={<NotFound />} />
            </Routes>
          </div>
        </BrowserRouter>
      </ThemeProvider>
    </AuthorizationContext.Provider>
  );
}

export default App;
